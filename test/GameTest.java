package test;


import org.junit.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import src.Card;
import src.Dealer;
import src.Game;
import src.Player;
import src.enums.Suit;
import src.enums.Value;

import java.util.ArrayList;
import java.util.List;

public class GameTest {
    @Test
    public void dealInitialCardsTest() {
        Dealer dealer = new Dealer();
        List<Player> players = new ArrayList<>(List.of(new Player("Player 1"), new Player("Player 2"), new Player("Player 3")));
        Game game = new Game(dealer, players);
        game.dealInitialCards();
        for (Player player : players) {
            assertEquals(2, player.getCards().size());
        }
    }

    @Test
    public void declaresWinnerWhenPlayerGets21() {
        Player player = new Player("Player 1");
        player.receiveCard(new Card(Suit.CLUB, Value.ACE));
        player.receiveCard(new Card(Suit.CLUB, Value.TEN));
        List<Player> players = new ArrayList<>(List.of(player));
        Game game = new Game(new Dealer(), players);
        game.runGame();
        assertEquals(player, game.getWinner());
    }

    @Test
    public void declaresWinnerWhenOnlyOneLeft() {
        Dealer dealerMock = mock(Dealer.class);
        when(dealerMock.dealCard())
                .thenReturn(new Card(Suit.CLUB, Value.ACE))//p1
                .thenReturn(new Card(Suit.DIAMOND, Value.ACE))//p2
                .thenReturn(new Card(Suit.HEART, Value.ACE))//p1 = bust
                .thenReturn(new Card(Suit.CLUB, Value.TWO)); //p2 = will stay
        Player player = new Player("Player 1");
        Player player2 = new Player("Player 2");
        List<Player> players = new ArrayList<>(List.of(player, player2));
        Game game = new Game(dealerMock, players);
        game.runGame();
        assertEquals(player2, game.getWinner());
        assertEquals(1, players.size());
        assertEquals(player2, players.get(0));
    }

    @Test
    public void declaresWinnerFromPlayersInStickQueue() {

    }


}
