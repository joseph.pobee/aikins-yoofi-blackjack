package test;

import org.junit.Test;
import src.Card;
import src.Dealer;

import java.util.Collections;
import java.util.HashSet;


import static org.junit.jupiter.api.Assertions.*;


public class DealerTest {


    //Check card is not in card anymore after dealing

    @Test
    public void produces52UniqueCards() {
        Dealer aikins = new Dealer();
        assertEquals(52, new HashSet<>(aikins.getCards()).size());
    }

    @Test
    public void removeDealtCardFromDeck() {
        Dealer aikins = new Dealer();
        Card dealtCard = aikins.dealCard();

        assertEquals(0, Collections.frequency(aikins.getCards(), dealtCard));
    }


}
