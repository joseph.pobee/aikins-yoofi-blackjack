package test;

import org.junit.jupiter.api.Test;
import src.*;
import src.enums.Action;
import src.enums.Suit;
import src.enums.Value;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {

    @Test
    void updatesCurrentScoreWhenReceivesCard() {
        Player yoofi = new Player("Yoofi");
        Card cardOne = new Card(Suit.DIAMOND, Value.ACE);
        Card cardTwo = new Card(Suit.DIAMOND, Value.TWO);
        int expectedValue = Value.ACE.getValue() + Value.TWO.getValue();
        yoofi.receiveCard(cardOne);
        yoofi.receiveCard(cardTwo);
        assertEquals(expectedValue, yoofi.getCurrentScore());
    }

    @Test
    void updatesCardListWhenReceivesCard() {
        Player yoofi = new Player("Yoofi");
        Card cardOne = new Card(Suit.DIAMOND, Value.ACE);
        yoofi.receiveCard(cardOne);
        assertEquals(1, Collections.frequency(yoofi.getCards(), cardOne));
    }

    @Test
    void returnsCorrectNextAction() {
        Player yoofi = new Player("Yoofi");
        Card cardOne = new Card(Suit.DIAMOND, Value.ACE);
        yoofi.receiveCard(cardOne);
        assertEquals(Action.Hit, yoofi.getNextAction());

        Card cardTwo = new Card(Suit.HEART, Value.SEVEN);
        yoofi.receiveCard(cardTwo);
        assertEquals(Action.Stick, yoofi.getNextAction());
    }
}