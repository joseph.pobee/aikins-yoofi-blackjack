package src;

import src.enums.Action;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Game {
    private static final int BLACKJACK = 21;
    private final Dealer dealer;
    private final List<Player> players;
    private final Queue<Player> stickQueue;
    private Player winner;

    private final static int CARDS_PER_USER  = 2;


    //constructor
    public Game(Dealer dealer, List<Player> players) {
        stickQueue = new LinkedList<>();
        this.dealer = dealer;
        this.players = players;
    }


    public void dealInitialCards() {
        for (int count = 0; count < CARDS_PER_USER; count++) {
            for (Player player : players) {
                player.receiveCard(dealer.dealCard());
            }
        }
    }

    public Player getWinner() {
        return winner;
    }

    //methods
    public void declareWinner() {
        System.out.printf("%s won \n", winner);
    }

    public void runGame() {
        for (Player player : players) {
            player.showDealtCards();
            checkWinner(player);
        }
        if (winner != null) {
            declareWinner();
            return;
        }
        while (winner == null && players.size() > 0) {
            List<Player> currentPlayers = new ArrayList<>(players);
            for (Player player : currentPlayers) {
                Action action = player.getNextAction();
                switch (action) {
                    case Hit -> {
                        player.receiveCard(dealer.dealCard());
                        player.showDealtCards();
                        System.out.println(player + " Hit");
                        checkWinner(player);
                    }
                    case Stick -> {
                        stickQueue.add(player);
                        players.remove(player);
                        System.out.println(player + " Stick");
                        checkWinner();
                    }
                    case Bust -> {
                        players.remove(player);
                        System.out.println(player + " Bust");
                        checkWinner();
                    }
                }
                if (winner != null) {
                    declareWinner();
                    return;
                }
            }
        }
    }

    public void checkWinner(Player player) {
        if (player.getCurrentScore() == BLACKJACK) {
            winner = player;
        }
    }

    public void checkWinner() {
        if (players.size() == 1 && stickQueue.size() == 0) {
            winner = players.get(0);
        } else if (players.size() == 0  && stickQueue.size() > 0) {
            int maxScore = 0;
            for (Player player : stickQueue) {
                if (player.getCurrentScore() > maxScore) {
                    maxScore = player.getCurrentScore();
                    winner = player;
                }
            }

        }
    }
}
