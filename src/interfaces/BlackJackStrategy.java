package src.interfaces;

import src.enums.Action;

public interface BlackJackStrategy {
    static final int HIT_MINIMUM = 17;
    static final int BUST_MINIMUM = 21;

    Action decideAction(int currentScore);
}

