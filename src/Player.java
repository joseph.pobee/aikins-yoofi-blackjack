package src;

import src.bjkstrategies.AlwaysStick;
import src.bjkstrategies.BasicStrategy;
import src.enums.Action;
import src.interfaces.BlackJackStrategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Player {

    private final String name;

    private BlackJackStrategy strategy;

    private int currentScore;
    private List<Card> cards;

    public Player(String name, AlwaysStick strategy) {
        this.name = name;
        this.strategy = strategy;
        this.cards = new ArrayList<>();
    }

    public Player(String name) {
        this.name = name;
        this.strategy = new BasicStrategy();
    }

    //methods
    public List<Card> getCards() {
        return Collections.unmodifiableList(cards);
    }

    @Override
    public String toString() {
        return name;
    }

    public int getCurrentScore() {
        return currentScore;
    }

    public void receiveCard(Card card) {
        cards.add(card);
        currentScore += card.getValue();
    }

    public Action getNextAction() {
        return this.strategy.decideAction(currentScore);
    }

    public void showDealtCards() {
        System.out.println(name + " is dealt : ");
        for (Card card : cards) {
            System.out.println(card);
        }
        System.out.println();
    }
}
