package src.enums;

public enum Suit {
    HEART, CLUB, SPADE, DIAMOND;
}
