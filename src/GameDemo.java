package src;

import java.util.ArrayList;
import java.util.List;

class GameParameters {
    public int numberOfUsers;

    public GameParameters(int numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
    }
}

class ArgumentParser {
    public static GameParameters parseArguments(String[] args) throws InvalidArgumentException {
        if(args.length == 0) return new GameParameters(3);
        if(args.length == 1) {
            int numberOfUsers = Integer.parseInt(args[0]);
            if(numberOfUsers < 0 || numberOfUsers > 6) throw new InvalidArgumentException();
            return new GameParameters(numberOfUsers);
        }
        return new GameParameters(3);
    }
}


public class GameDemo {
    public static void main(String[] args) {
        try {
            GameParameters parameters = ArgumentParser.parseArguments(args);
            List<Player> players = new ArrayList<>();
            for (int i = 0; i < parameters.numberOfUsers; ++i) {
                players.add(new Player("player" + i));
            }
            Game game = new Game(new Dealer(), players);
            game.dealInitialCards();
            game.runGame();
        } catch (InvalidArgumentException e) {
            System.err.println(e);
        }
    }
}
