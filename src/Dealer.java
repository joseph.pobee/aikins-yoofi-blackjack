package src;

import src.enums.Suit;
import src.enums.Value;

import java.util.Collections;
import java.util.Stack;

public class Dealer {
    private final Stack<Card> cards;

    public Dealer() {
        this.cards = generateCards();
        shuffleCards();
    }

    private Stack<Card> generateCards() {
        Stack<Card> cards = new Stack<>();

        for (Suit suit : Suit.values()) {
            for (Value value : Value.values()) {
                cards.push(new Card(suit, value));
            }
        }
        return cards;
    }

    private void shuffleCards() {
        Collections.shuffle(cards);
    }

    public Card dealCard() {
        return cards.pop();
    }

    public Stack<Card> getCards() {
        return cards;
    }
}
