package src.bjkstrategies;

import src.enums.Action;
import src.interfaces.BlackJackStrategy;

public class AlwaysHit implements BlackJackStrategy {
    @Override
    public Action decideAction(int currentScore) {
        return Action.Hit;
    }
}
