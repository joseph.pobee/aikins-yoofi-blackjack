package src.bjkstrategies;

import src.enums.Action;
import src.interfaces.BlackJackStrategy;

public class AlwaysStick implements BlackJackStrategy {
    public String name;

    @Override
    public Action decideAction(int currentScore) {
        return Action.Stick;
    }
}
