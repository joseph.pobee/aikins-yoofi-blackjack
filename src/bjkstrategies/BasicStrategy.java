package src.bjkstrategies;

import src.enums.Action;
import src.interfaces.BlackJackStrategy;

public class BasicStrategy implements BlackJackStrategy {
    @Override
    public Action decideAction(int currentScore) {
        if (currentScore < HIT_MINIMUM) return Action.Hit;
        else if (currentScore > BUST_MINIMUM) return Action.Bust;
        else return Action.Stick;
    }
}


